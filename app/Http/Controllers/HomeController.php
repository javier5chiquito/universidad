<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
            $projects = DB::table('projects')
                    ->select(
                        'name as titulo',
                        'description as descripcion')
                    ->paginate(6); // consulta de proyectos para el micrositio

        return view('welcome')->with('projects',$projects);
    }
}
