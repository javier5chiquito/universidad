<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projects;

class ProjectsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
      $this->authorize('view');//no mostrar si no esta autorizado
      $projects = Projects::AllProjects();
      return view('programas.index',compact('projects'));
      //return view('provider.index',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$projects = new Projects;
    	$projects->name = $request['name'];
        $projects->description = $request['description'];
        //$projects->faculty = $request['faculty'];
        //$projects->campus = $request['campus'];
        $projects->status = $request['status'];
    	$projects->save();

      return response()->json(['status' => 200,'data' => $projects]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Projects $projects)
    {
        //return view('provider.edit',compact('provider'));
        //dd($students);
        return response()->json(['status' => 200,'data' => $projects]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Projects $projects)
    {
      //
       $projects->name = $request['name'];
       $projects->description = $request['description'];
       $projects->status = $request['status'];
       $projects->update();

      return response()->json(['status' => 200,'data' => $projects]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Projects::find($id)->delete();
      
      return response()->json(['status'=>200]);
    }
}
