<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;

class AdminController extends Controller
{
    //
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function home()
    {
        //
        $projects = DB::table('projects')
                ->select(
                  DB::raw('name as name'),
                  DB::raw('count(*) as number'))
                ->groupBy('name')
                ->get(); //consulta de todos los 

        $array[] = ['Name', 'Number']; //array para el grafico

        foreach($projects as $key => $value)
        {
           $array[++$key] = [$value->name, $value->number];
        }
        
     return view('home')->with('projects', json_encode($array));
    }

    public function index()
    {
        //consulta de usuarios que son rol admin
      $admin = User::AdminUsers();
      return view('administradores.index',compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //
        $this->validate($request, ['name' => 'required','email' => 'required|email|unique:users,email','password' => 'same:confirm-password']);

    	$admin = new User;
        $admin->name = $request['name'];
        $admin->email = $request['email'];
        $admin->password = Hash::make($request['password']);

        $admin->save();
        $admin->assignRole('Administrador');

      return response()->json(['status' => 200,'data' => $admin]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //return view('provider.edit',compact('provider'));
        //dd($students);
       $admin = User::find($request['idAdmin']);
      
      return response()->json(['status' => 200,'data' => $admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $admin = User::find($request['idAdmin']);
        $admin->name = $request['name'];
        $admin->email = $requestt['email'];
        $admin->password = Hash::make($request['password']);
        $admin->save();

      return response()->json(['status' => 200,'data' => $admin]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Funcion para eliminar user
       $user = User::find($id)->delete();
       return response()->json(200);
    }
}
