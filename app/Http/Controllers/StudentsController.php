<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;
use App\User;
use Illuminate\Support\Facades\Hash;


class StudentsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      //
      $this->authorize('view',new Students);//no mostrar si no esta autorizado
      
      $students = Students::AllStudents();
      return view('estudiantes.index',compact('students'));
      //return view('provider.index',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //creacion de datos del estudiante
    	$student = new Students;
    	$student->names = $request['names'];
    	$student->surnames = $request['surnames'];
        $cod=mt_rand(111111,999999); //d¿generar codigo no consecutivo del estudiante
    	$student->code_student = $cod;
        $student->age = $request['age'];
    	$student->gender = $request['gender'];
    	$student->city_reside = $request['city_reside'];
        $student->address = $request['address'];
        $student->city_birth = $request['city_birth'];
        $student->nationality = $request['nationality'];
    	$student->phone = $request['num_phone'];
    	//$student->email = $request['email'];

        //creacion del usuario para el estudiante
        
        $user = new User;
        $user->name = $request['names'].' '.$request['surnames'];
        $user->email = $request['email'];
        $user->password = Hash::make('12345678');//contraseña por defecto
        $user->save();
        $user->assignRole('Estudiantes');

        $student->user_id = $user->id;
    	$student->save();

      return response()->json(['status' => 200,'data' => $student]); //devolver funcion realizada
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Students $students)
    {
        //return view('provider.edit',compact('provider'));
        //dd($students);
        return response()->json(['status' => 200,'data' => $students]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Students $students)
    {
      //
    	$students->names = $request['names'];
        $student->surnames = $request['surnames'];
       // $student->code_student = $request['code_student']; el codigo del estudiante no se puede editar
        $student->age = $request['age'];
        $student->gender = $request['gender'];
        $student->city_reside = $request['city_reside'];
        $student->address = $request['address'];
        $student->city_birth = $request['city_birth'];
        $student->nationality = $request['nationality'];
        $student->phone = $request['num_phone'];
        //$student->email = $request['email']; // solo el correo de los datos
        
        $user = User::find($student->user_id);
        $user->name = $request['names'].' '.$request['surnames'];
        $user->email = $request['email'];
        $user->save();

        $students->update();

      return response()->json(['status' => 200,'data' => $students]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Students::find($id)->delete();
       return response()->json(['status'=>200]);
    }
}
