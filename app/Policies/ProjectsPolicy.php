<?php

namespace App\Policies;

use App\Projects;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Projects  $projects
     * @return mixed
     */
    public function view(User $user, Projects $projects)
    {
       //policy para entrar a la vista solo

       return $user->hasPermissionTo('Mod Admin');//validar que sea solo si tiene permiso de modulo admin
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
      //policy para entrar a la vista solo

       return $user->hasPermissionTo('Mod Admin');//validar que sea solo si tiene permiso de modulo admin
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Projects  $projects
     * @return mixed
     */
    public function update(User $user, Projects $projects)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Projects  $projects
     * @return mixed
     */
    public function delete(User $user, Projects $projects)
    {
      //policy para entrar a la vista solo

       return $user->hasPermissionTo('Mod Admin');//validar que sea solo si tiene permiso de modulo admin
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Projects  $projects
     * @return mixed
     */
    public function restore(User $user, Projects $projects)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Projects  $projects
     * @return mixed
     */
    public function forceDelete(User $user, Projects $projects)
    {
       //policy para entrar a la vista solo

       return $user->hasPermissionTo('Mod Admin');//validar que sea solo si tiene permiso de modulo admin
    }
}
