<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Teachers extends Model
{
    //

    public function scopeAllTeachers($query)
    {
        $data = DB::table('teachers')
                   ->orderBy('created_at','desc')
                   ->paginate(5);

        return $data;
    }

}
