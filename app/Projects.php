<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Projects extends Model
{
    //funcion scope que me trae todos los proyectos de la tabla proyectos
    public function scopeAllProjects($query)
    {
        $data = DB::table('projects')
                  ->orderBy('created_at','desc')
                  ->paginate(5);

        return $data;
    }
}
