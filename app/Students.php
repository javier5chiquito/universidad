<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Students extends Model
{
    //funcion scope que me trae todos los estudiantes de la tabla estudiantes
    public function scopeAllStudents($query)
    {
        $data = DB::table('students')
                   ->orderBy('created_at','desc')
                   ->paginate(5);//paginados de 5 en 5

        return $data;
    }

}
