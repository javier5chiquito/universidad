<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use DB;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //funcion scope que me trae todos los usuarios con rol de admin
    public function scopeAdminUsers($query)
    {
        $data = DB::table('model_has_roles')
                ->join('roles','roles.id','=','model_has_roles.role_id')
                ->join('users','users.id','=','model_has_roles.model_id')
                ->where('roles.name','Administrador')
                ->select(
                    'roles.name as rol',
                    'users.name as name',
                    'users.id as id',
                    'users.email as email')
                //->groupBy('users.name')
                ->orderBy('users.created_at','desc')
                ->paginate(5);

        return $data;
    }

    public function scopeStudentUsers($query)
    {
        $data = DB::table('model_has_roles')
                ->join('roles','roles.id','=','model_has_roles.role_id')
                ->join('users','users.id','=','model_has_roles.model_id')
                ->where('roles.name','Estudiantes')
                ->select(
                    'roles.name as rol',
                    'users.name as name',
                    'users.id as id',
                    'users.email as email')
                //->groupBy('users.name')
                ->orderBy('users.created_at','desc')
                ->paginate(5);

        return $data;
    }

    public function scopeStudentData($query)
    {
        $data = DB::table('students')
                ->join('users','users.id','=','students.user_id')
                ->where('students.user_id',$this->id)
                ->select(
                    'users.name as name',
                    'users.id as id',
                    'users.email as email',
                    'students.phone as phone')
                //->groupBy('users.name')
                ->first();

        return $data;
    }

}
