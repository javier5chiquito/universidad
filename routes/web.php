<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'AdminController@home')->name('home');
Route::get('/','HomeController@index');

// Rutas de cursos 
Route::get('/proyectos/index', 'ProjectsController@index');
Route::get('/proyecto/create', 'ProjectsController@create');
Route::post('/proyecto', 'ProjectsController@store')->name('new-project');
Route::get('/proyecto/{project}/edit', 'ProjectsController@edit');
Route::put('/proyecto/{project}', 'ProjectsController@update');
Route::delete('/proyecto/{project}', 'ProjectsController@destroy');

// Rutas de admin
Route::get('/admin/index', 'AdminController@index');
Route::get('/admin/create', 'AdminController@create');
Route::post('/admin', 'AdminController@store')->name('new-admin');
Route::get('/admin/{Admin}/edit', 'AdminController@edit');
Route::put('/admin/{Admin}', 'AdminController@update');
Route::delete('/admin/{Admin}', 'AdminController@destroy');

// Rutas de estudiantes
Route::get('/estudiantes/index', 'StudentsController@index');
Route::get('/estudiante/create', 'StudentsController@create');
Route::post('estudiante', 'StudentsController@store')->name('new-student');
Route::get('/estudiante/{students}/edit', 'StudentsController@edit');
Route::put('/estudiante/{students}', 'StudentsController@update');
Route::delete('/estudiante/{students}', 'StudentsController@destroy');
