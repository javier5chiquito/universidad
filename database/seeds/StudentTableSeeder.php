<?php

use Illuminate\Database\Seeder;
use App\Students;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Students::create([
        	'names' => 'Rosa Emilia',
        	'surnames' => 'Romero Montilla',
        	'identification' => '25511824',
        	'gender' => 'F',
        	"date_birth" => '1994-11-04',
        	'num_phone' => '3452134674',
        	'email' => 'rosa@student.com',
        	'degree' => 'Ingeniero Civil',
        	'adress' => 'Calle 56 #7-10, Bogotá',
        ]);

        Students::create([
        	'names' => 'Jose Fernando',
        	'surnames' => 'Cardenas Perez',
        	'identification' => '22608589',
        	'gender' => 'M',
        	"date_birth" => '1991-05-07',
        	'num_phone' => '3452133862',
        	'email' => 'arnaldo@teacher.com',
        	'degree' => 'Licenciado en Letras',
        	'adress' => 'Calle 72 #24-30, Bogotá',
        ]);

        Students::create([
        	'names' => 'Manuel Alejandro',
        	'surnames' => 'Aure',
        	'identification' => '23643313',
        	'gender' => 'M',
        	"date_birth" => '1993-08-28',
        	'num_phone' => '3452564667',
        	'email' => 'ivan@teacher.com',
        	'degree' => 'Economista',
        	'adress' => 'Tv. 91 #7-11, Bogotá',
        ]);

        Students::create([
        	'names' => 'Edixon David',
        	'surnames' => 'Barrera Garcia',
        	'identification' => '24578226',
        	'gender' => 'M',
        	"date_birth" => '1990-02-04',
        	'num_phone' => '3459214624',
        	'email' => 'julian@teacher.com',
        	'degree' => 'Licenciado en Matematica',
        	'adress' => 'Carrera 56 #10-20, Bogotá',
        ]);
    }
}
