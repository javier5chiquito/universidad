<?php

use Illuminate\Database\Seeder;
use App\Courses;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Courses::create([
        	'description' => 'Mecánica de Fluidos',
        ]);

        Courses::create([
        	'description' => 'Castellano',
        ]);

        Courses::create([
        	'description' => 'Introducción a la Economía',
        ]);

        Courses::create([
        	'description' => 'Matemática I',
        ]);
    }
}
