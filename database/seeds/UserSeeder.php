<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //crear usuarios con sus permisos
       $RoleAdmin = Role::create(['name' => 'Administrador']);// Rol administradoor

       $RoleStudent = Role::create(['name' => 'Estudiantes']); // Rol de estudiante

       //asignar permisos a los usuarios
       $RoleAdmin->givePermissionTo('Mod Admin');//Asignado a rol administradoor el permiso de modulo admin y modulo estudiante
       
       $RoleStudent->givePermissionTo('Mod Estudiante'); //Asignado a rol estudiante el permiso de modulo estudiante

       //usuarios de prueba
       $Admin = new User();
       $Admin->name ='Admin.uno'; // nombre del usuario admin
       $Admin->email ='admin@admin.com'; // correo del usuario admin
       $Admin->password = bcrypt('12345678');
       $Admin->save();
       $Admin->assignRole($RoleAdmin);// asignacion del rol al usuario admin

       //Usuario estudiante
       $User = new User();
       $User->name ='Estudiante.uno'; //nombre usuario estudiante
       $User->email ='user@user.com';
       $User->password = bcrypt('12345678');
       $User->save();
       $User->assignRole($RoleStudent); //asignacion del rol estudiante al usuario

       //segundo estudiante de proeba
       $UserTwo = new User();
       $UserTwo->name ='Estudiante.dos';
       $UserTwo->email ='user.dos@user.com';
       $UserTwo->password = bcrypt('12345678');
       $UserTwo->save();
       $UserTwo->assignRole($RoleStudent);

    }
}
