<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //creacion de permisos
       Permission::create(['name' => 'Mod Admin']); //permiso midulo administradoor
       
       Permission::create(['name' => 'Mod Estudiante']); //permiso modulo estudiantes

    }
}
