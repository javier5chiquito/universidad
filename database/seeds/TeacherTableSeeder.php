<?php

use Illuminate\Database\Seeder;
use App\Teachers;

class TeacherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Teachers::create([
        	'names' => 'Julio Alexander',
        	'surnames' => 'Perez Mejia',
        	'identification' => '22578324',
        	'gender' => 'M',
        	'num_phone' => '3452134674',
        	'email' => 'julio@teacher.com',
        	'degree' => 'Ingeniero Civil',
        	'adress' => 'Calle 56 #7-10, Bogotá',
        ]);

        Teachers::create([
        	'names' => 'Jose Arnaldo',
        	'surnames' => 'Montilla Espinoza',
        	'identification' => '20528304',
        	'gender' => 'M',
        	'num_phone' => '3452133862',
        	'email' => 'arnaldo@teacher.com',
        	'degree' => 'Licenciado en Letras',
        	'adress' => 'Calle 72 #24-30, Bogotá',
        ]);

        Teachers::create([
        	'names' => 'Ivan Rafael',
        	'surnames' => 'Contreras Labrador',
        	'identification' => '21723343',
        	'gender' => 'M',
        	'num_phone' => '3452564667',
        	'email' => 'ivan@teacher.com',
        	'degree' => 'Economista',
        	'adress' => 'Tv. 91 #7-11, Bogotá',
        ]);

        Teachers::create([
        	'names' => 'Julian David',
        	'surnames' => 'Romero Castro',
        	'identification' => '22578324',
        	'gender' => 'M',
        	'num_phone' => '3459214624',
        	'email' => 'julian@teacher.com',
        	'degree' => 'Licenciado en Matematica',
        	'adress' => 'Carrera 56 #10-20, Bogotá',
        ]);
    }
}
