@extends('layouts.app')
@section('content')

        <!--MDB Inputs-->
        <div class="container mt-4">

          <!-- Grid row -->
           <div class="row">
            
                <!-- Grid column -->
                <div class="col-md-12">
            
                    <div class="card">
                      <div class="card-body">

                        <h5 class="pt-3 pb-2 font-up col-md-push-6"><strong> Nuevo Administrador </strong></h5>
                        <hr>

                         <form id="fr-profesor" method="POST" action="{{route('new-admin')}}">
                            <!-- Grid row -->
                             @csrf
                            
                            <input name="idAdmin" type="hidden" id="id">
                            
                            <div class="row mt-2 mt-2">
                              <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Nombres</label>
                                    <input name="name" type="text" id="nombres" class="form-control validate">
                                  </div>
                                </div>
                              <!-- Grid column -->
                              
                              <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Email</label>
                                    <input name="email" type="email" id="email" class="form-control validate">
                                  </div>
                                </div>
                              <!-- Grid column -->

                            </div>
                            <!-- Grid row mt-2 -->
                             
                            <!-- Grid row mt-2 -->
                            <div class="row mt-2 mt-2">
                              <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Contraseña de usuario</label>
                                    <input name="password" type="password" id="password" class="form-control validate">
                                  </div>
                            
                                </div>
                              <!-- Grid column -->
                              
                              <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Confirmar Contraseña</label>
                                    <input name="password_confirmation" type="password" id="password-confirm" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->
                            
                            </div>

                            <div class="row mt-2 mr-2 float-right"> 
                             <p class="">
                              <button id="guardar" class="btn btn-secondary">  Guardar </button>
                             <!-- botones para editar un registro o cancelarlo -->
                              <button id="p-guardar" class="btn btn-info" style="display: none;">  Actualizar </button>
                              <button id="cancelar" class="btn btn-danger" style="display: none;"> Cancelar </button>
                             </p>
                            </div>
                          </form>
                        </div>
                    </div>
            
                </div>
                <!-- Grid column -->
            
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="row mt-3">
            
              <!-- Grid column -->
              <div class="col-md-12">
            
                  <div class="card">
                    <div class="card-body">
                     <h5 class="pt-3 pb-2 font-up col-md-push-6"><strong>Administradores </strong></h5>
                  <div class="table-responsive">

                
              <table id="TablaProfesores" class="table table-bordred table-striped">
                   
                  <thead class="table-info"> 
                   <th><input name="" type="checkbox" id="checkall" /></th>
                    <th>Nombres</th>
                    <th>Email</th>
                    <th>Editar</th>  
                    <th>Borrar</th>
                  </thead>
                   
                   <tbody> 
                    @foreach($admin as $value)
                    <tr class="Col{{$value->id}}">
                     <td><input type="checkbox" class="checkthis" /></td>
                     <td>{{$value->name}}</td>
                     <td>{{($value->email}}</td>
                     <td><a class="btn btn-primary btn-xs" id="editar-p" data-url="{{url('admin/'.$value->id.'/edit')}}" data-title="Editar" >Editar</a></td>
                     <td class="Delete{{$value->id}}"><a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-p" data-url="{{url('admin/'.$value->id)}}" data-id="{{$value->id}}">Borrar</a></td>
                    </tr>
                    @endforeach

                   </tbody>
                </table>
                  {{ $admin->links() }}
               </div>    

                        </div>
                    </div>
            
                </div>
                <!-- Grid column -->
            </div>
        </div>
    <!--MDB Inputs-->
 @include('modales.modal_borrar')
@endsection