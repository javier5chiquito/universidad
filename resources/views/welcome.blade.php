<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

         <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style type="text/css">
            body {
                min-height: 75rem;
                padding-top: 4.5rem;
                margin-bottom: 60px;
            }

            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                color: black;
                background-color: #3490dc !important;
                text-align: center;
                padding-left: 16px;
                padding-right: 16px;
            }
        </style>
    </head>
    <body>

            <div id="app">
                <nav class="navbar navbar-expand-md navbar-light bg-primary shadow-sm fixed-top">
                    <div class="container">
                        <a class="navbar-brand" href="{{ url('/') }}">
                           {{ config('app.name', 'Laravel') }}
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            <ul class="navbar-nav mr-auto">

                            </ul>

                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav ml-auto">
                                <!-- Authentication Links -->
                                @guest
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                {{-- @if (Route::has('register'))
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                  </li>
                                @endif --}}

                                @else
                                   
                                   <li class="nav-item">
                                    <a class="nav-link" href="{{ route('home') }}">{{ __('Home') }}</a>
                                   </li>
                                @endguest
                            </ul>
                        </div>
                    </div>
                </nav>

                <main class="container">   
                      <div class="p-4 p-md-5 mb-4 text-white rounded bg-dark mt-3">
                    <div class="col-md-6">
                        <h1 class="display-5 font-italic">Sistema Universidad</h1>
                        <p class="lead my-3">Sistema universidad mira nuestros</p>
                        <p class="lead mb-0"><a href="#" class="text-white fw-bold">Mire nuestros proyectos</a></p>
                    </div>
                </div>

                    <div class="row mb-2">
                       <!-- aqui empieza la coleccion de proyectos -->
                       @foreach($projects as $value)
                        <div class="col-md-6">
                            <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative mr-0">
                                <div class="col p-4 d-flex flex-column position-static">
                                    <img src="https://cdn.pixabay.com/photo/2016/02/19/11/19/office-1209640_960_720.jpg" class="img-fluid">
                                    <h3 class="mb-0 mt-4">{{$value->titulo}}</h3>
                                    <p class="card-text mb-auto">{{$value->descripcion}}</p>
                                </div>
                                <div class="col-auto d-none d-lg-block"></div>
                            </div>
                        </div>
                        @endforeach
                    <!-- aqui termina la coleccion de proyectos -->
                    </div>
                <hr>
                <br>
                {{ $projects->links() }}
                </main>           
            </div>

        <footer class="footer">
            <div class="container p-2">            
                <p></p>
            </div>
        </footer>
    </body>
</html>