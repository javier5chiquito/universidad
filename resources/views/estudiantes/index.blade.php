@extends('layouts.app')
@section('content')

        <!--MDB Inputs-->
        <div class="container mt-4">

          <!-- Grid row -->
           <div class="row">
            
                <!-- Grid column -->
                <div class="col-md-12">
            
                    <div class="card">
                      <div class="card-body">

                        <h5 class="pt-3 pb-2 font-up col-md-push-6"><strong> Nuevo Estudiante </strong></h5>
                        <hr>

                         <form id="fr-estudiante" method="POST" action="{{route('new-student')}}">
                            <!-- Grid row -->
                             @csrf
                            
                            <input name="idProf" type="hidden" id="id">
                            
                            <div class="row mt-2 mt-2">
                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Nombres</label>
                                    <input name="names" type="text" id="nombres" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Apellidos</label>
                                    <input name="surnames" type="text" id="apellidos" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                            </div>
                            <!-- Grid row mt-2 -->
                             
                            <!-- Grid row mt-2 -->
                            <div class="row mt-2 mt-2">
                              <!-- Grid column -->
                                <div class="col-md-6" style="display: none;">
                                  <div class="md-form">
                                    <label for="form2" class="">Codigo estudiante</label>
                                    <input name="code_student" type="text" id="codigo" class="form-control">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6" style="">
                                  <div class="md-form">
                                    <label for="form2" class="">Edad</label>
                                    <input name="age" type="text" id="edad" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Sexo</label>
                                    <select name="gender" class="form-control validate" id="sexo">
                                      <option value=""> </option>
                                      <option value="M">Masculino </option>
                                      <option value="F">Femenino </option>
                                    </select>
                                  </div>
                            
                                </div>
                                <!-- Grid column -->
                            
                            </div>
                            <!-- Grid row -->
      
                            <div class="row mt-2 mt-2">

                              <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Ciudad de residencia</label>
                                    <input name="city_reside" type="text" id="residencia" class="form-control">
                                  </div>
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Dirección</label>
                                    <input name="address" type="text" id="direccion" class="form-control">
                                  </div>
                                </div>
                                <!-- Grid column -->
                            
                            </div>
                            <!-- Grid row -->

                            <div class="row mt-2 mt-2">

                              <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Ciudad de nacimiento</label>
                                    <input name="city_birth" type="text" id="nacimiento" class="form-control">
                                  </div>
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Nacionalidad</label>
                                    <input name="nationality" type="text" id="nacionalidad" class="form-control">
                                  </div>
                                </div>
                                <!-- Grid column -->
                            
                            </div>
                            <!-- Grid row -->

                            <div class="row mt-2">
                              <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Teléfono</label>
                                    <input name="num_phone" min="0" type="number" id="telefono" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Email</label>
                                    <input name="email" type="email" id="email" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row -->
                            <div class="row mt-2 mr-2 float-right"> 
                             <p class="">
                              <button id="guardar" class="btn btn-secondary">  Guardar </button>
                             <!-- botones para editar un registro o cancelarlo -->
                              <button id="e-guardar" class="btn btn-info" style="display: none;">  Actualizar </button>
                              <button id="cancelar" class="btn btn-danger" style="display: none;"> Cancelar </button>
                             </p>
                            </div>
                          </form>
                        </div>
                    </div>
            
                </div>
                <!-- Grid column -->
            
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="row mt-3">
            
              <!-- Grid column -->
              <div class="col-md-12">
            
                  <div class="card">
                    <div class="card-body">
                     <h5 class="pt-3 pb-2 font-up col-md-push-6"><strong>Estudiantes </strong></h5>
                  <div class="table-responsive">

                
              <table id="TablaEstudiantes" class="table table-bordred table-striped">
                   
                  <thead class="table-info"> 
                   <th><input name="" type="checkbox" id="checkall" /></th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Codigo</th>
                    <th>Email</th>
                    <th>Telefono</th>
                    <th>Editar</th>  
                    <th>Borrar</th>
                  </thead>
                   
                   <tbody> 
                    @foreach($students as $value)
                    <tr class="Col{{$value->id}}">
                     <td><input type="checkbox" class="checkthis" /></td>
                     <td>{{$value->names}}</td>
                     <td>{{$value->surnames}}</td>
                     <td>{{$value->code_student}}</td>
                     <td>{{(!empty($value->email))?$value->email:''}}</td>
                     <td>{{(!empty($value->phone))?$value->phone:''}}</td>
                     <td><a class="btn btn-primary btn-xs" id="editar-e" data-url="{{url('estudiante/'.$value->id.'/edit')}}" data-title="Editar" >Editar</a></td>
                     <td class="Delete{{$value->id}}"><a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-e" data-url="{{url('estudiante/'.$value->id)}}" data-id="{{$value->id}}">Borrar</a></td>
                    </tr>
                    @endforeach

                   </tbody>
                </table>
                  {{ $students->links() }}

               </div>    

                        </div>
                    </div>
            
                </div>
                <!-- Grid column -->
            </div>
        </div>
    <!--MDB Inputs-->
 @include('modales.modal_borrar')
@endsection