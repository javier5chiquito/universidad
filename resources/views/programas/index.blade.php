@extends('layouts.app')
@section('content')

        <!--MDB Inputs-->
        <div class="container mt-4">

          <!-- Grid row -->
           <div class="row">
            
                <!-- Grid column -->
                <div class="col-md-12">
            
                    <div class="card">
                      <div class="card-body">

                        <h5 class="pt-3 pb-2 font-up col-md-push-6"><strong> Agregar proyectos</strong></h5>
                        <hr>

                         <form id="fr-cursos" method="POST" action="{{route('new-project')}}" enctype="multipart/form-data">
                            <!-- Grid row -->
                             @csrf
                            
                            <input name="idCur" type="hidden" id="id">
                            
                            <div class="row mt-2 mt-2">
                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Nombre</label>
                                    <input name="name" type="text" id="nombre" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Descripcion</label>
                                    <input name="description" type="text" id="descripcion" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                            </div>

                            <div class="row mt-2 mt-2">
                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Imagen</label>
                                    <input name="image" accept="image/*" type="file" id="imagen" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Estado</label>
                                    <select name="status" class="form-control validate" id="estado">
                                      <option value=""> </option>
                                      <option value="A">Activo </option>
                                      <option value="I">Inactivo </option>
                                    </select>
                                  </div>
                            
                                </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row mt-2 -->
                             
                            <!-- Grid row -->
                            <div class="row mt-2 mr-2 float-right"> 
                             <p class="">
                              <button id="guardar" class="btn btn-secondary">  Guardar </button>
                             <!-- botones para editar un registro o cancelarlo -->
                              <button id="c-guardar" class="btn btn-info" style="display: none;">  Actualizar </button>
                              <button id="cancelar" class="btn btn-danger" style="display: none;"> Cancelar </button>
                             </p>
                            </div>
                          </form>
                        </div>
                    </div>
            
                </div>
                <!-- Grid column -->
            
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="row mt-3">
            
              <!-- Grid column -->
              <div class="col-md-12">
            
                  <div class="card">
                    <div class="card-body">
                     <h5 class="pt-3 pb-2 font-up col-md-push-6"><strong>Proyectos </strong></h5>
                  <div class="table-responsive">

                
              <table id="TablaCursos" class="table table-bordred table-striped">
                   
                  <thead class="table-info"> 
                   <th><input name="" type="checkbox" id="checkall" /></th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Editar</th>  
                    <th>Borrar</th>
                  </thead>
                   
                   <tbody> 
                    @foreach($projects as $value)
                    <tr class="Col{{$value->id}}">
                     <td><input type="checkbox" class="checkthis" /></td>
                     <td>{{$value->name}}</td>
                     <td>{{$value->description}}</td>
                     <td><a class="btn btn-primary btn-xs" id="editar-c" data-url="{{url('proyecto/'.$value->id.'/edit')}}" data-title="Editar" >Editar</a></td>
                     <td class="Delete{{$value->id}}"><a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-c" data-url="{{url('proyecto/'.$value->id)}}" data-id="{{$value->id}}">Borrar</a></td>
                    </tr>
                    @endforeach

                   </tbody>
                </table>
                  {{ $projects->links() }}

               </div>    

                        </div>
                    </div>
            
                </div>
                <!-- Grid column -->
            </div>
        </div>
    <!--MDB Inputs-->
 @include('modales.modal_borrar')
@endsection