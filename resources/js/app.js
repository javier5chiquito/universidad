require('./bootstrap');

$(document).ready(function() {
/***** Para estudiantes ********************************/
$("#fr-estudiante").on("click","#guardar",function(e){
          e.preventDefault();

  var control = true;
  var arr = $(".validate");

	  if (arr.length > 0) {
	    for (i = 0; i < arr.length; i++) {
	      if (arr[i].value === '' || arr[i].value === null || arr[i].value === " ") {
	          //alert("El campo '"+  arr[i].id + "' no puede estar vacio"); //Si quieres que imprima en específico que campo no puede dejar vacío
	          
	          $("#"+arr[i].id).css("border", "red solid 1px"); 
	          $("#"+arr[i].id).siblings('span').remove(); 
	          $("#"+arr[i].id).after("<span class='text-danger'>Este campo es obligatorio</span>"); 

	          control = false;
	         // break;
	      }
	    }
	  }

  if (control) {
     var datosRegistro = $("#fr-estudiante").serialize();
     var formUrl = $("#fr-estudiante").attr("action");
     var rows = "";

     $.ajax({
       type: "POST",
       url:  formUrl,
       data: datosRegistro,
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){

       	  //alert('Registro guardado');
       	   $("#fr-estudiante")[0].reset();
       	   swal("Correcto!",'Registro Guardado', "success");
       	}

       	value = data.data;

       	//insertar nuevo registro en la tabla
       	rows = rows + '<tr id="Col'+value.id+'">';
	  	rows = rows + '<td></td>';
	  	rows = rows + '<td>'+value.names+'</td>';
	  	rows = rows + '<td>'+value.surnames+'</td>';
	  	rows = rows + '<td>'+value.identification+'</td>';
	  	rows = rows + '<td>'+value.email+'</td>';
	  	rows = rows + '<td>'+value.num_phone+'</td>';
        rows = rows + '<td data-id="'+value.id+'">';
        rows = rows + '<a class="btn btn-primary btn-xs" id="editar-e" data-url="{{url("estudiante/'+value.id+'/edit")}}" data-title="Editar" data-id="'+value.id+'">Editar</a> ';
        rows = rows + '</td>';
	  	rows = rows + '<td class="Delete'+value.id+'">';
        rows = rows + '<a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-e" data-toggle="modal" data-url="{{url("estudiante/'+value.id+')}}" data-target="#delete" data-id="'+value.id+'">Borrar</a>';
        rows = rows + '</td>';
	  	rows = rows + '</tr>';

       $("#TablaEstudiantes tbody").append(rows);

      }//fin del success
     });
  }else{

  	alert("Faltan campos por llenar"); //o el mensaje para todos
  }

});

//voy a editar un registro

$("#TablaEstudiantes").on("click","#editar-e",function(e){
          e.preventDefault();

  var objet = $(this);
  var id = $(this).data('id');
  var rows = $(this).parents('td').parents('tr');
  var url = $(this).data('url');

     $.ajax({
       type: "GET",
       url:  url,
       data: {"id":id},
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){
       	 console.log(data.data);

       	 values = data.data; //simplificar el resultado

       	  $("#fr-estudiante")[0].reset();
       	  
       	  $("#fr-estudiante #nombres").val(values.names);
       	  $("#fr-estudiante #apellidos").val(values.surnames);
       	  $("#fr-estudiante #identificacion").val(values.identification);
       	  $("#fr-estudiante #sexo").val(values.gender);
       	  $("#fr-estudiante #nacimiento").val(values.date_birth);
       	  $("#fr-estudiante #direccion").val(values.address);
       	  $("#fr-estudiante #telefono").val(values.num_phone);
       	  $("#fr-estudiante #email").val(values.email);
       	  $("#fr-estudiante #id").val(values.id);

       	  //ahora los botones que sirven para editar

       	  $("#fr-estudiante #guardar").hide();//ocultar el de guardado

       	  $("#fr-estudiante #e-guardar").show();//mostrar actualizar
       	  $("#fr-estudiante #cancelar").show();//mostrar cancelar

       	   objet.hide();//deshabilito el boton de editar de la tabla
       	   $("#TablaEstudiantes .Delete"+values.id).hide();//ocultar el de delete

       	   rows.hide();

       	 // swal("Done!",'Registro Actualizado', "success");
       	}

      }//fin del success
     });

});

   //cancelo el editar y refresco para 
    $("#fr-estudiante").on("click","#cancelar",function(e){
          e.preventDefault();

        window.location.reload();

    });
  // funcion llena tablas con los registros ya actualizados

$("#fr-estudiante").on("click","#e-guardar",function(e){
          e.preventDefault();

    var control = true;
    var arr = $(".validate");
    var objet = $("#fr-estudiante #id").val();

	  if (arr.length > 0) {
	    for (i = 0; i < arr.length; i++) {
	      if (arr[i].value === '' || arr[i].value === null || arr[i].value === " ") {
	          //alert("El campo '"+  arr[i].id + "' no puede estar vacio"); //Si quieres que imprima en específico que campo no puede dejar vacío
	          
	          $("#"+arr[i].id).css("border", "red solid 1px"); 
	          $("#"+arr[i].id).siblings('span').remove(); 
	          $("#"+arr[i].id).after("<span class='text-danger'>Este campo es obligatorio</span>"); 

	          control = false;
	         // break;
	      }
	    }
	  }

  if (control) {

     var datosRegistro = $("#fr-estudiante").serialize();
     var formUrl = $("#fr-estudiante").attr("action");
     var rows = "";

     formUrl = formUrl+'/'+objet; //reescribiendo url para editar

     $.ajax({
       type: "PUT",
       url:  formUrl,
       data: datosRegistro,
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){

       	 // alert('Registro Actualizado');

       	   $("#fr-estudiante")[0].reset();
       	   $("#fr-estudiante #id").val();

       	   swal("Correcto!",'Registro Guardado', "success");

       	   $("#Col"+objet).remove();

       	   value = data.data;

       	//insertar nuevo registro en la tabla
       	rows = rows + '<tr id="Col'+value.id+'">';
	  	rows = rows + '<td></td>';
	  	rows = rows + '<td>'+value.names+'</td>';
	  	rows = rows + '<td>'+value.surnames+'</td>';
	  	rows = rows + '<td>'+value.identification+'</td>';
	  	rows = rows + '<td>'+value.email+'</td>';
	  	rows = rows + '<td>'+value.num_phone+'</td>';
	  	rows = rows + '<td data-id="'+value.id+'">';
        rows = rows + '<a class="btn btn-primary btn-xs" id="editar-e" data-url="{{url("estudiante/'+value.id+'/edit")}}" data-title="Editar" data-id="'+value.id+'">Editar</a> ';
        rows = rows + '</td>';
	  	rows = rows + '<td class="Delete'+value.id+'">';
        rows = rows + '<a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-e" data-toggle="modal" data-url="{{url("estudiante/'+value.id+')}}" data-target="#delete" data-id="'+value.id+'">Borrar</a> ';
        rows = rows + '</td>';
	  	rows = rows + '</tr>';


         $("#TablaEstudiantes tbody").append(rows);
         $("#fr-estudiante #guardar").show();//ocultar el de guardado

       	  $("#fr-estudiante #e-guardar").hide();//mostrar actualizar
       	  $("#fr-estudiante #cancelar").hide();//mostrar cancelar
       	}

      }//fin del success
     });
  }else{

  	alert("Faltan campos por llenar"); //o el mensaje para todos
  }

});

  //cancelo el editar y refresco para 
    $("#TablaEstudiantes").on("click","#borrar-e",function(e){
                e.preventDefault();

        url = $(this).data('url');
        objet = $(this).parents('td').parents('tr');

	       	   objet.remove();

        $.ajax({
	       type: "DELETE",
	       url:  url,
	       beforeSend: function(){
	       },
	       success: function(data) {

	       	 if(data.status == "200"){
	       	  //alert('Registro guardado');
	       	   $("#fr-estudiante")[0].reset();
	       	   swal("Correcto!",'Se borro el reguistro', "success");
	       	 }
       	   }

         //window.location.reload();
        });
    });
  // funcion llena tablas con los registros ya actualizados

/***********Para profesores  ******************************************************/

$("#fr-profesor").on("click","#guardar",function(e){
          e.preventDefault();

  var control = true;
  var arr = $(".validate");

	  if (arr.length > 0) {
	    for (i = 0; i < arr.length; i++) {
	      if (arr[i].value === '' || arr[i].value === null || arr[i].value === " ") {
	          //alert("El campo '"+  arr[i].id + "' no puede estar vacio"); //Si quieres que imprima en específico que campo no puede dejar vacío
	          
	          $("#"+arr[i].id).css("border", "red solid 1px"); 
	          $("#"+arr[i].id).siblings('span').remove(); 
	          $("#"+arr[i].id).after("<span class='text-danger'>Este campo es obligatorio</span>"); 

	          control = false;
	         // break;
	      }
	    }
	  }

  if (control) {
     var datosRegistro = $("#fr-profesor").serialize();
     var formUrl = $("#fr-profesor").attr("action");
     var rows = "";

     $.ajax({
       type: "POST",
       url:  formUrl,
       data: datosRegistro,
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){

       	  //alert('Registro guardado');
       	   $("#fr-profesor")[0].reset();
       	   swal("Correcto!",'Registro Guardado', "success");
       	}

       	value = data.data;

       	//insertar nuevo registro en la tabla
       	rows = rows + '<tr id="Col'+value.id+'">';
	  	rows = rows + '<td></td>';
	  	rows = rows + '<td>'+value.names+'</td>';
	  	rows = rows + '<td>'+value.surnames+'</td>';
	  	rows = rows + '<td>'+value.degree+'</td>';
	  	rows = rows + '<td>'+value.identification+'</td>';
	  	rows = rows + '<td>'+value.email+'</td>';
	  	rows = rows + '<td>'+value.num_phone+'</td>';
	  	rows = rows + '<td data-id="'+value.id+'">';
        rows = rows + '<a class="btn btn-primary btn-xs" id="editar-p" data-url="{{url("profesor/'+value.id+'/edit")}}" data-title="Editar" data-id="'+value.id+'">Editar</a> ';
        rows = rows + '</td>';
	  	rows = rows + '<td class="Delete'+value.id+'">';
        rows = rows + '<a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-p" data-url="{{url("profesor/'+value.id+')}}" data-id="'+value.id+'">Borrar</a> ';
        rows = rows + '</td>';
        rows = rows + '</td>';
	  	rows = rows + '</tr>';

       $("#TablaProfesores tbody").append(rows);

      }//fin del success
     });
  }else{

  	alert("Faltan campos por llenar"); //o el mensaje para todos
  }

});

//voy a editar un registro

$("#TablaProfesores").on("click","#editar-p",function(e){
          e.preventDefault();

  var objet = $(this);
  var id = $(this).data('id');
  var rows = $(this).parents('td').parents('tr');
  var url = $(this).data('url');

     $.ajax({
       type: "GET",
       url:  url,
       data: {"id":id},
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){
       	 console.log(data.data);

       	 values = data.data; //simplificar el resultado

       	  $("#fr-profesor")[0].reset();
       	  
       	  $("#fr-profesor #nombres").val(values.names);
       	  $("#fr-profesor #apellidos").val(values.surnames);
       	  $("#fr-profesor #identificacion").val(values.identification);
       	  $("#fr-profesor #sexo").val(values.gender);
       	  $("#fr-profesor #titulo").val(values.degree);
       	  $("#fr-profesor #direccion").val(values.address);
       	  $("#fr-profesor #telefono").val(values.num_phone);
       	  $("#fr-profesor #email").val(values.email);
       	  $("#fr-profesor #id").val(values.id);

       	  //ahora los botones que sirven para editar

       	  $("#fr-profesor #guardar").hide();//ocultar el de guardado

       	  $("#fr-profesor #p-guardar").show();//mostrar actualizar
       	  $("#fr-profesor #cancelar").show();//mostrar cancelar

       	   objet.hide();//deshabilito el boton de editar de la tabla
       	   $("#TablaProfesores .Delete"+values.id).hide();//ocultar el de delete

       	   rows.hide();

       	 // swal("Done!",'Registro Actualizado', "success");
       	}

      }//fin del success
     });

});

   //cancelo el editar y refresco para 
    $("#fr-profesor").on("click","#cancelar",function(e){
          e.preventDefault();

        window.location.reload();

    });
  // funcion llena tablas con los registros ya actualizados

$("#fr-profesor").on("click","#p-guardar",function(e){
          e.preventDefault();

    var control = true;
    var arr = $(".validate");
    var objet = $("#fr-profesor #id").val();

	  if (arr.length > 0) {
	    for (i = 0; i < arr.length; i++) {
	      if (arr[i].value === '' || arr[i].value === null || arr[i].value === " ") {
	          //alert("El campo '"+  arr[i].id + "' no puede estar vacio"); //Si quieres que imprima en específico que campo no puede dejar vacío
	          
	          $("#"+arr[i].id).css("border", "red solid 1px"); 
	          $("#"+arr[i].id).siblings('span').remove(); 
	          $("#"+arr[i].id).after("<span class='text-danger'>Este campo es obligatorio</span>"); 

	          control = false;
	         // break;
	      }
	    }
	  }

  if (control) {

     var datosRegistro = $("#fr-profesor").serialize();
     var formUrl = $("#fr-profesor").attr("action");
     var rows = "";

     formUrl = formUrl+'/'+objet; //reescribiendo url para editar

     $.ajax({
       type: "PUT",
       url:  formUrl,
       data: datosRegistro,
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){

       	 // alert('Registro Actualizado');

       	   $("#fr-profesor")[0].reset();
       	   $("#fr-profesor #id").val();

       	   swal("Correcto!",'Registro Guardado', "success");

       	   $("#Col"+objet).remove();

       	   value = data.data;

       	//insertar nuevo registro en la tabla
       	rows = rows + '<tr id="Col'+value.id+'">';
	  	rows = rows + '<td></td>';
	  	rows = rows + '<td>'+value.names+'</td>';
	  	rows = rows + '<td>'+value.surnames+'</td>';
	  	rows = rows + '<td>'+value.degree+'</td>';
	  	rows = rows + '<td>'+value.identification+'</td>';
	  	rows = rows + '<td>'+value.email+'</td>';
	  	rows = rows + '<td>'+value.num_phone+'</td>';
	  	rows = rows + '<td data-id="'+value.id+'">';
        rows = rows + '<a class="btn btn-primary btn-xs" id="editar-p" data-url="{{url("profesor/'+value.id+'/edit")}}" data-title="Editar" data-id="'+value.id+'">Editar</a> ';
        rows = rows + '</td>';
	  	rows = rows + '<td class="Delete'+value.id+'">';
        rows = rows + '<a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-p" data-url="{{url("profesor/'+value.id+')}}" data-id="'+value.id+'">Borrar</a> ';
        rows = rows + '</td>';
	  	rows = rows + '</tr>';

         $("#TablaProfesores tbody").append(rows);
         $("#fr-profesor #guardar").show();//ocultar el de guardado

       	  $("#fr-profesor #p-guardar").hide();//mostrar actualizar
       	  $("#fr-profesor #cancelar").hide();//mostrar cancelar
       	}

      }//fin del success
     });
  }else{

  	alert("Faltan campos por llenar"); //o el mensaje para todos
  }

});

  //cancelo el editar y refresco para 
    $("#TablaProfesores").on("click","#borrar-p",function(e){
             e.preventDefault();

        url = $(this).data('url');
        objet = $(this).parents('td').parents('tr');

	       	   objet.remove();

        $.ajax({
	       type: "DELETE",
	       url:  url,
	       beforeSend: function(){
	       },
	       success: function(data) {

	       	 if(data.status == "200"){
	       	  //alert('Registro guardado');
	       	   $("#fr-profesor")[0].reset();
	       	   swal("Correcto!",'Se borro el reguistro', "success");
	       	 }
       	   }

         //window.location.reload();
        });
    });
  // funcion llena tablas con los registros ya actualizados


 /***********Para Semestres  ******************************************************/

$("#fr-semestre").on("click","#guardar",function(e){
          e.preventDefault();

    var control = true;
    var arr = $(".validate");

	  if (arr.length > 0) {
	    for (i = 0; i < arr.length; i++) {
	      if (arr[i].value === '' || arr[i].value === null || arr[i].value === " ") {
	          //alert("El campo '"+  arr[i].id + "' no puede estar vacio"); //Si quieres que imprima en específico que campo no puede dejar vacío
	          
	          $("#"+arr[i].id).css("border", "red solid 1px"); 
	          $("#"+arr[i].id).siblings('span').remove(); 
	          $("#"+arr[i].id).after("<span class='text-danger'>Este campo es obligatorio</span>"); 

	          control = false;
	         // break;
	      }
	    }
	  }

  if (control) {
     var datosRegistro = $("#fr-semestre").serialize();
     var formUrl = $("#fr-semestre").attr("action");
     var rows = "";

     $.ajax({
       type: "POST",
       url:  formUrl,
       data: datosRegistro,
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){
       	  //alert('Registro guardado');
       	  $("#fr-semestre")[0].reset();
       	  swal("Correcto!",'Registro Guardado', "success");
       	}

       	value = data.data;

       	//insertar nuevo registro en la tabla
      rows = rows + '<tr id="Col'+value.id+'">';
	  	rows = rows + '<td></td>';
	  	rows = rows + '<td>'+value.start_date+'</td>';
	  	rows = rows + '<td>'+value.end_date+'</td>';
	  	rows = rows + '<td data-id="'+value.id+'">';
      rows = rows + '<a class="btn btn-primary btn-xs" id="editar-p" data-url="{{url(semestre/'+value.id+'/edit")}}" data-title="Editar" data-id="'+value.id+'">Editar</a> ';
      rows = rows + '</td>';
	  	rows = rows + '<td class="Delete'+value.id+'">';
      rows = rows + '<a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-p" data-url="{{url(semestre/'+value.id+')}}" data-id="'+value.id+'">Borrar</a> ';
      rows = rows + '</td>';
	  	rows = rows + '</tr>';

       $("#TablaSemestres tbody").append(rows);

      }//fin del success
     });
  }else{

  	alert("Faltan campos por llenar"); //o el mensaje para todos
  }

});

//voy a editar un registro

$("#TablaSemestres").on("click","#editar-s",function(e){
          e.preventDefault();

  var objet = $(this);
  var id = $(this).data('id');
  var rows = $(this).parents('td').parents('tr');
  var url = $(this).data('url');

     $.ajax({
       type: "GET",
       url:  url,
       data: {"id":id},
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){
       	 //console.log(data.data);

       	 values = data.data; //simplificar el resultado

         console.log(values);

       	  $("#fr-semestre")[0].reset();
       	  
       	  $("#fecha_inicio").val(values.start_date);
       	  $("#fecha_fin").val(values.end_date);
       	  $("#fr-semestre #id").val(values.id);

       	  //ahora los botones que sirven para editar

       	  $("#fr-semestre #guardar").hide();//ocultar el de guardado

       	  $("#fr-semestre #s-guardar").show();//mostrar actualizar
       	  $("#fr-semestre #cancelar").show();//mostrar cancelar

       	   objet.hide();//deshabilito el boton de editar de la tabla
       	   $("#TablaSemestres .Delete"+values.id).hide();//ocultar el de delete

       	   rows.hide();

       	 // swal("Done!",'Registro Actualizado', "success");
       	}

      }//fin del success
     });

});

   //cancelo el editar y refresco para 
    $("#fr-semestre").on("click","#cancelar",function(e){
          e.preventDefault();

        window.location.reload();

    });
  // funcion llena tablas con los registros ya actualizados

$("#fr-semestre").on("click","#s-guardar",function(e){
          e.preventDefault();

    var control = true;
    var arr = $(".validate");
    var objet = $("#fr-semestre #id").val();

	  if (arr.length > 0) {
	    for (i = 0; i < arr.length; i++) {
	      if (arr[i].value === '' || arr[i].value === null || arr[i].value === " ") {
	          //alert("El campo '"+  arr[i].id + "' no puede estar vacio"); //Si quieres que imprima en específico que campo no puede dejar vacío
	          
	          $("#"+arr[i].id).css("border", "red solid 1px"); 
	          $("#"+arr[i].id).siblings('span').remove(); 
	          $("#"+arr[i].id).after("<span class='text-danger'>Este campo es obligatorio</span>"); 

	          control = false;
	         // break;
	      }
	    }
	  }

  if (control) {

     var datosRegistro = $("#fr-semestre").serialize();
     var formUrl = $("#fr-semestre").attr("action");
     var rows = "";

     formUrl = formUrl+'/'+objet; //reescribiendo url para editar

     $.ajax({
       type: "PUT",
       url:  formUrl,
       data: datosRegistro,
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){

       	 // alert('Registro Actualizado');

       	  $("#fr-semestre")[0].reset();
       	  $("#fr-semestre #id").val();

       	   swal("Correcto!",'Registro Guardado', "success");

       	   $("#Col"+objet).remove();

       	   value = data.data;

       	//insertar nuevo registro en la tabla
      rows = rows + '<tr id="Col'+value.id+'">';
	  	rows = rows + '<td></td>';
	  	rows = rows + '<td>'+value.names+'</td>';
	  	rows = rows + '<td>'+value.surnames+'</td>';
	  	rows = rows + '<td data-id="'+value.id+'">';
      rows = rows + '<a class="btn btn-primary btn-xs" id="editar-s" data-url="{{url(semestre/'+value.id+'/edit)}}" data-title="Editar" data-id="'+value.id+'">Editar</a> ';
      rows = rows + '</td>';
	  	rows = rows + '<td class="Delete'+value.id+'">';
      rows = rows + '<a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-s" data-url="{{url(semestre/'+value.id+')}}" data-id="'+value.id+'">Borrar</a> ';
      rows = rows + '</td>';
	  	rows = rows + '</tr>';

         $("#TablaSemestres tbody").append(rows);
         $("#fr-semestre #guardar").show();//ocultar el de guardado

       	  $("#fr-semestre #s-guardar").hide();//mostrar actualizar
       	  $("#fr-semestre #cancelar").hide();//mostrar cancelar
       	}

      }//fin del success
     });
  }else{

  	alert("Faltan campos por llenar"); //o el mensaje para todos
  }

});

  //cancelo el editar y refresco para 
    $("#TablaSemestres").on("click","#borrar-s",function(e){
             e.preventDefault();

        url = $(this).data('url');
        objet = $(this).parents('td').parents('tr');

	       	objet.remove();

        $.ajax({
	       type: "DELETE",
	       url:  url,
	       beforeSend: function(){
	       },
	       success: function(data) {

	       	 if(data.status == "200"){
	       	  //alert('Registro guardado');
	       	   $("#fr-semestre")[0].reset();
	       	   swal("Correcto!",'Se borro el reguistro', "success");
	       	 }
       	   }

         //window.location.reload();
        });
    });
  // funcion llena tablas con los registros ya actualizados
 

 /***********Para Curssos ******************************************************/

$("#fr-cursos").on("click","#guardar",function(e){
          e.preventDefault();

    var control = true;
    var arr = $(".validate");

	  if (arr.length > 0) {
	    for (i = 0; i < arr.length; i++) {
	      if (arr[i].value === '' || arr[i].value === null || arr[i].value === " ") {
	          //alert("El campo '"+  arr[i].id + "' no puede estar vacio"); //Si quieres que imprima en específico que campo no puede dejar vacío
	          
	          $("#"+arr[i].id).css("border", "red solid 1px"); 
	          $("#"+arr[i].id).siblings('span').remove(); 
	          $("#"+arr[i].id).after("<span class='text-danger'>Este campo es obligatorio</span>"); 

	          control = false;
	         // break;
	      }
	    }
	  }

  if (control) {
     var datosRegistro = $("#fr-cursos").serialize();
     var formUrl = $("#fr-cursos").attr("action");
     var rows = "";

     $.ajax({
       type: "POST",
       url:  formUrl,
       data: datosRegistro,
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){

       	  //alert('Registro guardado');
       	 $("#fr-cursos")[0].reset();
       	 swal("Correcto!",'Registro Guardado', "success");
       	}

       	value = data.data;

       	//insertar nuevo registro en la tabla
      rows = rows + '<tr id="Col'+value.id+'">';
	  	rows = rows + '<td></td>';
	  	rows = rows + '<td>'+value.description+'</td>';
	  	rows = rows + '<td data-id="'+value.id+'">';
      rows = rows + '<a class="btn btn-primary btn-xs" id="editar-c" data-url="{{url(asignatura/'+value.id+'/edit)}}" data-title="Editar" data-id="'+value.id+'">Editar</a> ';
      rows = rows + '</td>';
	  	rows = rows + '<td class="Delete'+value.id+'">';
      rows = rows + '<a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-c" data-url="{{url(asignatura/'+value.id+')}}" data-id="'+value.id+'">Borrar</a> ';
      rows = rows + '</td>';
	  	rows = rows + '</tr>';

       $("#TablaCursos tbody").append(rows);

      }//fin del success
     });
  }else{

  	alert("Faltan campos por llenar"); //o el mensaje para todos
  }

});

//voy a editar un registro

$("#TablaCursos").on("click","#editar-c",function(e){
          e.preventDefault();

  var objet = $(this);
  var id = $(this).data('id');
  var rows = $(this).parents('td').parents('tr');
  var url = $(this).data('url');

     $.ajax({
       type: "GET",
       url:  url,
       data: {"id":id},
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){
       	 
       	 //console.log(data.data);

       	  values = data.data; //simplificar el resultado

       	  $("#fr-cursos")[0].reset();
       	  
       	  $("#fr-cursos #descripcion").val(values.description);
       	  $("#fr-cursos #id").val(values.id);

       	  //ahora los botones que sirven para editar

       	  $("#fr-cursos #guardar").hide();//ocultar el de guardado

       	  $("#fr-cursos #c-guardar").show();//mostrar actualizar
       	  $("#fr-cursos #cancelar").show();//mostrar cancelar

       	   objet.hide();//deshabilito el boton de editar de la tabla
       	   $("#TablaCursos .Delete"+values.id).hide();//ocultar el de delete

       	   rows.hide();

       	 // swal("Done!",'Registro Actualizado', "success");
       	}

      }//fin del success
     });

});

   //cancelo el editar y refresco para 
    $("#fr-cursos").on("click","#cancelar",function(e){
          e.preventDefault();

        window.location.reload();

    });
  // funcion llena tablas con los registros ya actualizados

$("#fr-cursos").on("click","#c-guardar",function(e){
          e.preventDefault();

    var control = true;
    var arr = $(".validate");
    var objet = $("#fr-cursos #id").val();

	  if (arr.length > 0) {
	    for (i = 0; i < arr.length; i++) {
	      if (arr[i].value === '' || arr[i].value === null || arr[i].value === " ") {
	          //alert("El campo '"+  arr[i].id + "' no puede estar vacio"); //Si quieres que imprima en específico que campo no puede dejar vacío
	          
	          $("#"+arr[i].id).css("border", "red solid 1px"); 
	          $("#"+arr[i].id).siblings('span').remove(); 
	          $("#"+arr[i].id).after("<span class='text-danger'>Este campo es obligatorio</span>"); 

	          control = false;
	         // break;
	      }
	    }
	  }

  if (control) {

     var datosRegistro = $("#fr-cursos").serialize();
     var formUrl = $("#fr-cursos").attr("action");
     var rows = "";

     formUrl = formUrl+'/'+objet; //reescribiendo url para editar

     $.ajax({
       type: "PUT",
       url:  formUrl,
       data: datosRegistro,
       beforeSend: function(){
       },
       success: function(data) {

       	if(data.status == "200"){

       	 // alert('Registro Actualizado');

       	   $("#fr-cursos")[0].reset();
       	   $("#fr-cursos #id").val();

       	   swal("Correcto!",'Registro Guardado', "success");

       	   $("#Col"+objet).remove();

       	   value = data.data;

       	//insertar nuevo registro en la tabla
      rows = rows + '<tr id="Col'+value.id+'">';
	  	rows = rows + '<td></td>';
	  	rows = rows + '<td>'+value.description+'</td>';
	  	rows = rows + '<td data-id="'+value.id+'">';
      rows = rows + '<a class="btn btn-primary btn-xs" id="editar-c" data-url="{{url(asignatura/'+value.id+'/edit)}}" data-title="Editar" data-id="'+value.id+'">Editar</a> ';
      rows = rows + '</td>';
	  	rows = rows + '<td class="Delete'+value.id+'">';
      rows = rows + '<a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-c" data-url="{{url(asignatura/'+value.id+')}}" data-id="'+value.id+'">Borrar</a> ';
      rows = rows + '</td>';
	  	rows = rows + '</tr>';

       $("#TablaCursos tbody").append(rows);
         $("#fr-cursos #guardar").show();//ocultar el de guardado

       	 $("#fr-cursos #c-guardar").hide();//mostrar actualizar
       	 $("#fr-cursos #cancelar").hide();//mostrar cancelar
      }

      }//fin del success
     });
  }else{

  	alert("Faltan campos por llenar"); //o el mensaje para todos
  }

});

  //cancelo el editar y refresco para 
    $("#TablaCursos").on("click","#borrar-c",function(e){
             e.preventDefault();

        url = $(this).data('url');
        objet = $(this).parents('td').parents('tr');

	       	objet.remove();

        $.ajax({
	       type: "DELETE",
	       url:  url,
	       beforeSend: function(){
	       },
	       success: function(data) {

	       	 if(data.status == "200"){
	       	  //alert('Registro guardado');
	       	   $("#fr-cursos")[0].reset();
	       	   swal("Correcto!",'Se borro el reguistro', "success");
	       	 }
       	   }

         //window.location.reload();
        });
    });
  // funcion llena tablas con los registros ya actualizados

});
